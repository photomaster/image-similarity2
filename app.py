from flask import Flask
from flask import render_template

from imgsim import image_pb2 as image__pb2
from imgsim.img_sim import ImageSimilarityServiceServicer
from imgsim.provider import ImageProvider

app = Flask(__name__)
image_provider = ImageProvider()
s = ImageSimilarityServiceServicer()


class Context:
    def __init__(self):
        self.code = 200
        self.details = ''

    def set_code(self, status):
        self.code = status

    def set_details(self, details):
        self.details = details


@app.route('/')
def index():
    return render_template('index.html', images=image_provider.get_images())


@app.route('/similar/<guid>')
def detail(guid):
    image = image_provider.get_image(guid)

    request = image__pb2.ImageRequest(
        image=image__pb2.Image(
            guid=image["guid"],
            path=image["path"]
        ),
        limit=20
    )

    ctx = Context()
    response = s.GetSimilar(request, ctx)
    similar_images = []

    if ctx.code == 200:
        similar_images = response.similarities
        similar_images = [
            {"guid": image.image.guid, "path": image.image.path, "name": image.image.name, "id": image.image.guid,
             "similarity": image.distance} for image in similar_images]

    return render_template('detail.html', similar_images=similar_images, image=image)
