FROM tensorflow/tensorflow:2.3.1

ENV PYTHONUNBUFFERED 1
ENV FLASK_APP app.py

RUN mkdir /code
WORKDIR /code

RUN apt update && apt install -y curl libpq-dev && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /code/
RUN pip install -r requirements.txt
    

RUN mkdir -p /code/tensorflow && \
    curl -o /code/tensorflow/model.tar.gz "http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz" && \
    cd /code/tensorflow && \
    tar -xzvf model.tar.gz classify_image_graph_def.pb && \
    rm model.tar.gz

COPY . /code/
RUN python -m grpc_tools.protoc -I proto --python_out=imgsim --grpc_python_out=imgsim proto/image.proto && \
    cd imgsim && sed -i -r 's/import (.+_pb2.*)/from . import \1/g' *_pb2*.py

ENTRYPOINT [ "./docker-entrypoint.sh" ]

EXPOSE 8080/tcp
