"""
init
"""

from yoyo import step

__depends__ = {}


def apply_step(conn):
    cursor = conn.cursor()
    cursor.execute("""
    CREATE TABLE photos(
         id  SERIAL PRIMARY KEY,
         guid    int not null,
         path          varchar(400)       NOT NULL,
         vector        float8[] DEFAULT NULL
    );""")
    cursor.execute("CREATE UNIQUE INDEX ON photos(path);")
    cursor.execute("CREATE UNIQUE INDEX ON photos(guid);")


def rollback_step(conn):
    cursor = conn.cursor()
    cursor.execute(
        "DROP TABLE photos"
    )

steps = [
    step(apply_step, rollback_step)
]
