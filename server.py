from concurrent import futures
import grpc
from imgsim import image_pb2_grpc
from imgsim.img_sim import ImageSimilarityServiceServicer


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    image_pb2_grpc.add_ImageSimilarityServiceServicer_to_server(ImageSimilarityServiceServicer(), server)
    server.add_insecure_port('[::]:8080')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
