#!/usr/bin/env bash
set -e

yoyo apply --database "postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB" ./migrations -b

pip install annoy --force --no-binary :all:

if [[ ! -f "test.ann" ]]; then
  python <<EOF
from imgsim.ml import create_annoy_index
from imgsim.provider import ImageProvider

image_provider = ImageProvider()
create_annoy_index([image["vector"] for image in image_provider.get_images()])
image_provider.close()
EOF
fi

if [[ "$@" == "flask" ]]; then
    export FLASK_APP=app.py
    flask run --host=0.0.0.0 --port=8080
else
  echo "starting grpc server..."
  python server.py
fi



