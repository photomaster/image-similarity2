import glob
import re

from imgsim.ml import create_annoy_index, run_inference_on_images
from imgsim.provider import ImageProvider

image_provider = ImageProvider()
files = sorted(glob.glob("static/**/*.png", recursive=True) + glob.glob("static/**/*.jpg", recursive=True))
vectors = run_inference_on_images(files)

for i, vec in enumerate(vectors):
    image_provider.save_image({
        "guid": i+1,
        "path": re.sub(r"^static", "", files[i]),
        "vector": vec
    })
create_annoy_index([image["vector"] for image in image_provider.get_images()])
image_provider.close()
