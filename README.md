# ImgSim 2 Test

## GRPC
Add some photos to `static` and start all containers `docker-compose up -d`.

To index photos use the evans cli tool:
```shell script
evans -p 8080 --proto proto/image.proto
> call AddImage
> guid (TYPE_INT32) => 2
> name (TYPE_STRING) => test photo
> path (TYPE_STRING) => static/test-photo.png

# repeat this step for each image
# now you can query the index for similar images:
> call SimilarImages
> image::guid (TYPE_INT32) => 1
> image::name (TYPE_STRING) => asd
> image::path (TYPE_STRING) => static/1985_israel/israel1_006.jpg
> limit (TYPE_INT32) => 10
```
## Flask (Debug Web Interface)
```shell script
docker-compose up -d pg
# execute the following command only if your database is empty
docker-compose run --rm --entrypoint "python import.py" web
docker-compose run --rm -p 8080:8080 web flask
```

