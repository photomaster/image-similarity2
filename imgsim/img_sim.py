import os

from annoy import AnnoyIndex
from grpc import StatusCode

import imgsim.image_pb2 as image_pb2
from imgsim.image_pb2_grpc import ImageSimilarityServiceServicer
from imgsim.ml import run_inference_on_images, create_annoy_index
from imgsim.provider import ImageProvider

MAX_LIMIT = 25
DISTANCE_THRESHOLD = 15

def get_similar(vector, limit=10):
    u = AnnoyIndex(2048, 'euclidean')
    u.load('test.ann')

    v = u.get_nns_by_vector(vector, limit, include_distances=True)
    u.unload()
    del u
    return v


class ImageSimilarityServiceServicer(ImageSimilarityServiceServicer):
    def __init__(self, image_provider=ImageProvider()):
        self.image_provider = image_provider

    def __del__(self):
        self.image_provider.close()

    def GetSimilar(self, request, context):
        image = self.image_provider.get_image(request.image.guid)
        if not image:
            try:
                image = self._import_image(request.image.path, guid=request.image.guid)
            except Exception as e:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f'Photo {request.image.path} not found!')
                return

        similar_ids, distances = get_similar(image["vector"], min(request.limit, MAX_LIMIT))
        images = self.image_provider.get_images(ids=similar_ids)
        images = [image_pb2.ImageSimilarity(
            image=image_pb2.Image(path=image["path"], guid=image["guid"]), distance=distances[i]
        ) for i, image in enumerate(images)]

        images = list(filter(lambda image: image.distance < DISTANCE_THRESHOLD, images))

        return image_pb2.ImageSimilarityResponse(
            similarities=images, count=len(images)
        )

    def AddImage(self, request, context):
        image = self.image_provider.get_image(request.guid)
        if not image:
            print("Adding image", request.path, request.guid)
            try:
                image = self._import_image(request.path, guid=request.guid)
            except:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f'Photo {request.path} not found!')
                return
        return image_pb2.Image(path=image["path"], guid=image["guid"])

    def _import_image(self, path, **kwargs):
        from multiprocessing import Process, Queue

        og_path = path
        if path[0] == "/":
            path = path[1:]
        path = os.path.join(os.getcwd(), os.getenv("MEDIA_ROOT", "static"), path)
        if not os.path.isfile(path):
            raise Exception('File does not exist: ' + path)

        def run(file_path, q):
            q.put(run_inference_on_images((file_path,)))

        # todo: we spawn a separate process to prevent a gigantic memory leak because of the GraphDef object.
        # If you happen to know how to release the memory, you are welcome to change this.
        # Fuck this shit...

        q = Queue()
        p = Process(target=run, args=(path, q,))
        p.start()
        vectors = q.get()
        p.join()
        image = self.image_provider.save_image({
            **kwargs,
            "path": og_path,
            "vector": vectors[0]
        })
        create_annoy_index([image["vector"] for image in self.image_provider.get_images()])
        return image
