"""
Original author Douglas Duhaime
https://douglasduhaime.com/posts/identifying-similar-images-with-tensorflow.html
"""

from annoy import AnnoyIndex
import os.path
import sys
import tarfile
import numpy as np
from six.moves import urllib
import tensorflow.compat.v1 as tf

dest_directory = "tensorflow"

DATA_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'


def create_annoy_index(vectors):
    t = AnnoyIndex(2048, 'euclidean')
    for i, vec in enumerate(vectors):
        # i+1 so that primary keys match index of vector
        t.add_item(i+1, vec)
    t.build(10)
    t.save("test.ann")
    t.unload()
    del t


def run_inference_on_images(image_list):
    maybe_download_and_extract()

    """Creates a graph from saved GraphDef file and returns a saver."""
    with tf.gfile.FastGFile(os.path.join(
            dest_directory, 'classify_image_graph_def.pb'), 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        graph_def = tf.import_graph_def(graph_def, name='')

    vectors = []

    with tf.Session() as sess:
        for image_index, image in enumerate(image_list):
            try:
                if not tf.gfile.Exists(image):
                    tf.logging.fatal('File does not exist %s', image)

                with tf.gfile.FastGFile(image, 'rb') as f:
                    image_data = f.read()
                    feature_tensor = sess.graph.get_tensor_by_name('pool_3:0')
                    feature_set = sess.run(feature_tensor,
                                           {'DecodeJpeg/contents:0': image_data})
                    feature_vector = np.squeeze(feature_set)
                    vectors.append(feature_vector)
            except:
                print('could not process image index', image_index, 'image', image)
                exit(1)

    return vectors


def maybe_download_and_extract():
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = DATA_URL.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.isfile(os.path.join(dest_directory, 'classify_image_graph_def.pb')):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (
                filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        statinfo = os.stat(filepath)
        print('Succesfully downloaded', filename, statinfo.st_size, 'bytes.')
        tarfile.open(filepath, 'r:gz').extractall(dest_directory)

