import os
import unittest
from glob import glob

import numpy as np
from annoy import AnnoyIndex
from imgsim.ml import create_annoy_index, run_inference_on_images


def get_index():
    t = AnnoyIndex(2048, "euclidean")
    t.load("test.ann")
    return t


class MlTestCase(unittest.TestCase):
    def tearDown(self):
        if os.path.isfile("test.ann"):
            os.remove("test.ann")

    def test_create_annoy_index(self):
        vectors = [np.zeros(2048), np.ones(2048), np.zeros(2048)]
        create_annoy_index(vectors)

        self.assertTrue(os.path.isfile("test.ann"))
        self.assertEqual(get_index().get_n_items(), len(vectors) + 1)

    def test_create_annoy_index_empty(self):
        create_annoy_index([])

        self.assertTrue(os.path.isfile("test.ann"))
        self.assertEqual(get_index().get_n_items(), 0)

    def test_run_inference_on_images_vectors(self):
        files = sorted(glob("test_sets/**/*.png") + glob("test_sets/**/*.jpg"))
        vectors = run_inference_on_images(files)

        self.assertEqual(len(vectors), len(files), "it should create a feature vector for each image")
        for v in vectors:
            self.assertEqual(len(v), 2048, 'a feature vector should have a length of 2048')

    def test_run_inference_on_images_vectors_jpg(self):
        files = sorted(glob("test_sets/**/*.jpg"))
        vectors = run_inference_on_images(files)

        self.assertEqual(len(vectors), len(files), "it should create a feature vector for each jpg image")

    def test_run_inference_on_images_vectors_png(self):
        files = sorted(glob("test_sets/**/*.png"))
        vectors = run_inference_on_images(files)

        self.assertEqual(len(vectors), len(files), "it should create a feature vector for each png image")

    def test_run_inference_on_images_vectors_gif(self):
        files = sorted(glob("test_sets/**/*.gif"))
        vectors = run_inference_on_images(files)

        self.assertEqual(len(vectors), len(files), "it should create a feature vector for each gif image")