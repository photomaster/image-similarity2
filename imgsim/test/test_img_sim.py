import glob
import unittest
from unittest.mock import patch
import numpy as np
from grpc import StatusCode
from grpc_testing import server_from_dictionary, strict_real_time
import imgsim.image_pb2 as image_pb2

from imgsim.img_sim import ImageSimilarityServiceServicer, DISTANCE_THRESHOLD


class MemoryImageProvider:
    def __init__(self):
        self.images = {}

    def get_image(self, guid):
        return self.images[guid] if guid in self.images else None

    def get_images(self, ids=None):
        if ids is not None:
            return filter(lambda image: image["id"] in ids, self.images.values())
        return self.images.values()

    def save_image(self, image):
        if not "id" in image:
            image["id"] = len(self.images.values()) + 1
        self.images[image["id"]] = image
        return image

    def close(self):
        pass


class ImageSimilarityServiceServicesTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.image_provider = MemoryImageProvider()
        servicers = {
            image_pb2.DESCRIPTOR.services_by_name['ImageSimilarityService']: ImageSimilarityServiceServicer(
                self.image_provider)
        }
        self.test_server = server_from_dictionary(
            servicers, strict_real_time())
        self.env = patch.dict('os.environ', {'MEDIA_ROOT': 'test_sets'})

    @patch("imgsim.img_sim.ImageSimilarityServiceServicer._import_image")
    def test_add_image_existing_image(self, _import_image):
        self.image_provider.save_image({"id": 1, "guid": 1, "path": "foobar.png", "name": ""})
        _import_image.return_value = {"id": 1, "guid": 1, "path": "foobar.png", "name": "", "vector": np.zeros(2048)}

        request = image_pb2.Image(
            guid=1,
            path="foobar.png"
        )
        response, metadata, code, details = self.call_rpc('AddImage', request)

        self.assertEqual(code, StatusCode.OK)
        self.assertEqual(response.guid, request.guid)
        self.assertEqual(response.path, request.path)
        _import_image.assert_not_called()

    def test_add_image(self):
        with self.env:
            request = image_pb2.Image(
                guid=1,
                path="/sea/beach-1852945_640.jpg"
            )

            response, metadata, code, details = self.call_rpc('AddImage', request)
            self.assertEqual(code, StatusCode.OK)
            self.assertEqual(response.guid, request.guid)
            self.assertEqual(response.path, request.path)
            self.assertEqual(len(self.image_provider.images), 1)

    def test_add_image_not_found(self):
        response, metadata, code, details = self.call_rpc('AddImage', image_pb2.Image(
            guid=1,
            path="I_dont_exist.jpg"
        ))
        self.assertEqual(code, StatusCode.NOT_FOUND)

    def test_get_similar_not_found(self):
        response, metadata, code, details = self.call_rpc("GetSimilar", image_pb2.ImageRequest(
                image=image_pb2.Image(guid=2, path="/I-dont-exist.jpg"),
                limit=2
        ))
        self.assertEqual(code, StatusCode.NOT_FOUND)

    def test_get_similar(self):
        with self.env:
            self.call_rpc("AddImage", image_pb2.Image(guid=1, path="/animals/tiger.gif"))
            self.call_rpc("AddImage", image_pb2.Image(guid=2, path="/animals/animal-1868911_640.png"))
            self.call_rpc("AddImage", image_pb2.Image(guid=3, path="/salzburg/salzburg-116768_640.jpg"))

            self.assertEqual(len(self.image_provider.images.values()), 3)

            response, metadata, code, details = self.call_rpc("GetSimilar", image_pb2.ImageRequest(
                image=image_pb2.Image(guid=2, path="/animals/animal-1868911_640.png"),
                limit=2
            ))
            self.assertEqual(code, StatusCode.OK)
            self.assertEqual(response.count, 2)
            self.assertEqual(len(response.similarities), 2)

            for similarity in response.similarities:
                self.assertIn(similarity.image.guid, (1, 2,), 'Should find the two photos containing a tiger')

    def test_get_similar__distance_threshold(self):
        with self.env:
            self.call_rpc("AddImage", image_pb2.Image(guid=1, path="/animals/tiger.gif"))
            self.call_rpc("AddImage", image_pb2.Image(guid=2, path="/animals/animal-1868911_640.png"))
            self.call_rpc("AddImage", image_pb2.Image(guid=3, path="/salzburg/salzburg-116768_640.jpg"))
            self.call_rpc("AddImage", image_pb2.Image(guid=4, path="/sea/beach-1852945_640.jpg"))

            response, metadata, code, details = self.call_rpc("GetSimilar", image_pb2.ImageRequest(
                image=image_pb2.Image(guid=2, path="/animals/animal-1868911_640.png"),
                limit=10
            ))

            for similarity in response.similarities:
                self.assertLessEqual(similarity.distance, DISTANCE_THRESHOLD, 'should not respect vector distance threshold')

    def call_rpc(self, method, request):
        method = self.test_server.invoke_unary_unary(
            method_descriptor=(image_pb2.DESCRIPTOR
                .services_by_name['ImageSimilarityService']
                .methods_by_name[method]),
            invocation_metadata={},
            request=request, timeout=5)

        return method.termination()
