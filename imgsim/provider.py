import os

import psycopg2


class ImageProvider:
    def __init__(self):
        self.conn = None

    def get_images(self, ids=None):
        con = self._get_connection()
        with con.cursor() as cur:
            if ids:
                cur.execute("SELECT id,guid,path,vector FROM photos WHERE id = ANY(%s) ORDER BY id ASC", (ids,))
            else:
                cur.execute("SELECT id,guid,path,vector FROM photos ORDER BY id ASC")
            return [{
                "id": data[0],
                "guid": data[1],
                "path": data[2],
                "vector": data[3]
            } for data in cur.fetchall()]

    def get_image(self, guid):
        con = self._get_connection()
        with con.cursor() as cur:
            cur.execute("SELECT id,guid,path,vector FROM photos WHERE guid=%s", (guid,))
            data = cur.fetchone()
            if not data:
                return None
            return {
                "id": data[0],
                "guid": data[1],
                "path": data[2],
                "vector": data[3]
            }

    def save_image(self, image):
        con = self._get_connection()
        with con.cursor() as cur:
            try:
                if 'id' in image:
                    cur.execute("UPDATE photos SET path = %s, guid = %s, vector = %s WHERE id = %s", (image["path"], image["guid"], image["vector"], image["id"],))
                    con.commit()
                else:
                    cur.execute("INSERT INTO photos (path, guid, vector) VALUES (%s, %s, %s) ON CONFLICT (path) DO UPDATE set guid=EXCLUDED.guid, vector=EXCLUDED.vector RETURNING id ", (image["path"], image["guid"], image["vector"].tolist(),))
                    image["id"] = cur.fetchone()[0]
                    con.commit()
            except Exception as e:
                print(e)
                cur.execute("ROLLBACK")

        return image

    def close(self):
        if self.conn:
            self.conn.close()

    def _get_connection(self):
        if not self.conn:
            self.conn = psycopg2.connect(
                "dbname={} user={} password={} host={}".format(os.getenv("POSTGRES_DB"),
                                                                      os.getenv("POSTGRES_USER"),
                                                                      os.getenv("POSTGRES_PASSWORD"), os.getenv("POSTGRES_HOST", "127.0.0.1")
                                                               ))
        return self.conn
